const httpApi = {}
import store from '@/store'
// import request from './request.js'

httpApi.get = ((url, params) => {
    let baseUrl = store.state.common.host
    let tokenInfo = store.getters['user/getTokenInfo']
    let token = (tokenInfo !== null && tokenInfo.token) ? tokenInfo.token : null
    return new Promise(resolve => {
        uni.request({
            url: baseUrl + url,
            data: params,
            method: 'get',
            header: {
                'token': token
            },
            success: (res) => {
				let result = res.data
			   if (result.code !== 1) {
				   if (result.code === 401) {
					   uni.redirectTo({
					       url: '/pages/tabBar/center/login'
					   })
				   }
			   } else {
				   resolve(result)
			   }
            },
		   
		    fail: (res) => {
			   console.log(res)
		    }
       })
    })
})

httpApi.upload = ((url, file) => {
    let baseUrl = store.state.common.host
    let tokenInfo = store.getters['user/getTokenInfo']
    let token = (tokenInfo !== null && tokenInfo.token) ? tokenInfo.token : null
    return new Promise(resolve => {
		uni.uploadFile({
			header: {
			    'token': token
			},
			url: baseUrl + url, //需传后台图片上传接口
			filePath: file[0],
			name: 'file',
			success: (res) => {
				let result = JSON.parse(res.data)
			   if (result.code !== 1) {
				   if (result.code === 401) {
					   uni.redirectTo({
					       url: '/pages/tabBar/center/login'
					   })
				   }
			   } else {
				   resolve(result)
			   }
			},
					   
			fail: (res) => {
			   console.log(res)
			}
		})
    })
})



/* httpApi.get = ((url, params) => {
	return new Promise(resolve => {
		request.get(url, params, true, true).then(res => { // 也可以使用配置的 baseURL 之外的 url，但是注意 url 路径要写完整
		    resolve(res)
		}).catch(e => 
		  console.error(e)
		)
	})
}) */

httpApi.post = ((url, params) => {
	let baseUrl = store.state.common.host
	let tokenInfo = store.getters['user/getTokenInfo']
	let token = (tokenInfo !== null && tokenInfo.token) ? tokenInfo.token : null
	return new Promise(resolve => {
		uni.request({
		    url: baseUrl + url, 
		    data: params,
			method: 'post',
		    header: {
		        'token': token 
		    },
		    success: (res) => {
				let result = res.data
				if (result.code !== 1) {
				   if (result.code === 401) {
					  /* uni.showToast({
						  icon: 'none',
						  title: result.message
					   }) */
					   uni.redirectTo({
						   url: '/pages/userCenter/login'
					   })
				   } else if (result.code === 0) {
					   uni.showToast({
						   icon: 'none',
					   	  title: result.message
					   })
				   }
				} else {
					resolve(result)
				}
		    },
			fail: (res) => {
				
			}
		})
	}) 
})


export default httpApi