import requestApi from './interceptor.js'
import store from '@/store'

const request = requestApi({ // 有效配置项只有三个
	baseURL: store.state.common.host, //baseURL
	timeout: 10, // 超时时间，单位毫秒。默认 60 秒
   // header: { 'x-custom-header': 'x-custom-header' }, // 设置请求头，建议放在请求拦截器中
	statusCode: [200, 401] // 服务器相应状态码为 200/401 时，网络请求不会 reject。也就是不会被 catch 到。如响应 401 时可以在响应拦截后 await 刷新 token + await 重新请求 + return response。即可实现无痛刷新。 
})

export default request


/**
 * 请求拦截
 */
request.interceptors.request.use(function (config) {
  let tokenInfo = store.getters['user/getTokenInfo']
  let token = (tokenInfo !== null && tokenInfo.access_token) ? tokenInfo.access_token : null
  if (token) {
    config.headers.token = token
  }
  return config
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})


/**
 * 响应拦截
 */
request.interceptors.response.use(function (response) {
  if (response.code !== 1) {
    // 对响应数据做点什么
    if (response.code === 401) {
      
    } else if (response.code === 0) {
        uni.showToast({
        	icon: 'none',
        	title: res.data.message
        })
    }
  }
  return response
}, function (error) {
  return Promise.reject(error)
})

