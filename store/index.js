import Vue from 'vue'
import Vuex from 'vuex'
import common from './modules/common'
import user from './modules/user'
import testPaperInfo from './modules/testPaperInfo'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    common,
	testPaperInfo,
	user
  },
  
  mutations: {
  
  },
})
